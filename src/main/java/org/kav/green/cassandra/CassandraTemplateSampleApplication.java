package org.kav.green.cassandra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassandraTemplateSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CassandraTemplateSampleApplication.class, args);
	}

}
