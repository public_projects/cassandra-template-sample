package org.kav.green.cassandra.main;

import lombok.extern.slf4j.Slf4j;
import org.kav.green.cassandra.entity.TargetEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.query.Criteria;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class Main implements CommandLineRunner {

    @Autowired
    private CassandraOperations cassandraOperations;

    @Override
    public void run(String... args) throws Exception {
        List<TargetEntity> targets = this.cassandraOperations
                                             .select(Query.query(Criteria.where("key")
                                                                         .is("7c638644-3203-11eb-82a5-40b034e3cf9d")), TargetEntity.class);
        log.info("Count: {}", targets.size());
    }
}
