package org.kav.green.cassandra.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;

@Setter
@Getter
@Table(value = "TP_Target", forceQuote = true)
public class TargetEntity implements Serializable {
    @PrimaryKeyColumn(name = "key", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String key;
    @PrimaryKeyColumn(name = "column1", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
    private String column1;
    @Column("value")
    private String value;
}
